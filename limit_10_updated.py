import pandas as pd
import os
from tkinter import filedialog

filename = filedialog.askopenfilename()
alerts = pd.read_csv(filename, header=0).sort_values(by=["timestamp"]).reset_index()

def max_10(alerts):
    hor = [4, 8, 12, 24, 36, 48, 60, 72, 168]
    thresh = [0.5, 1, 1.5, 2, 2.5, 3, 3.5, 4, 5]
    current_alerts = []
    alerts["min.from.start"] = 0
    alerts["end_time"] = 0
    alerts["result"] = 0
    alerts["sent"] = 0
    alerts["cumulative_gain"] = 0
    total_rows = len(alerts)
    for i in range(total_rows):
        h = hor.index(alerts.loc[i, "hor"])
        alerts.loc[i, "result"] = alerts.loc[i, "result_" + str(hor[h]) + "_" + str(thresh[h])]
        alerts.loc[i, "min.from.start"] = alerts.loc[i, "min.from.start_" + str(hor[h]) + "_" + str(thresh[h])]
        alerts.loc[i, "end_time"] = alerts.loc[i, "timestamp"] + (alerts.loc[i, "min.from.start"]*60)
        if len(current_alerts) < 10:
            current_alerts.append(i)
            alerts.loc[i, "sent"] = 1
            alerts.loc[i, "cumulative_gain"] = alerts.loc[i, "result"] + alerts.iloc[i-1]["cumulative_gain"]
        else:
            for a in current_alerts:
                if alerts.loc[i, "timestamp"] > alerts.loc[a, "end_time"]:
                    alerts.loc[i, "sent"] = 1
                    alerts.loc[i, "cumulative_gain"] = alerts.loc[i, "result"] + alerts.iloc[i-1]["cumulative_gain"]
                    current_alerts.remove(a)
                    current_alerts.append(i)
                    break
                else:
                    alerts.loc[i, "cumulative_gain"] = alerts.iloc[i - 1]["cumulative_gain"]
    alerts.to_csv(os.path.splitext(filename)[0] + "_analized.csv", index=False)
    return alerts["sent"].sum(), alerts.iloc[-1]["cumulative_gain"]

print(max_10(alerts))
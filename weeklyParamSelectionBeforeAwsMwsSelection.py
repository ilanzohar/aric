#import numpy as np
from typing import Any, Union

import pandas as pd

## this code inputs a csv of a configurations data base
## per given DAY (indexed here by week_index)  and horizon it selects the best configuration(s) by employing a greedy algorithm
## 3 rules are checked to get the next best configuration, always ignoring the best configuration thus reducing the
## number of configurations till none is left.
## the result is a column names 'selected' placed on the csv where 1 is annotated for optimal configuration
##
## written by Ilan Zohar
## February 2019
## Sparks A.B., Israel

horizon_lst =  [4, 8, 12, 24, 36, 48, 60, 72, 168] # was [4, 24, 48, 72, 168]
max_allowed_tx = {4: 70, 8: 60, 12: 70, 24: 50, 36: 50, 48: 20, 60: 25, 72: 20, 168: 30}
print("put in place restrictions on #allowed tx per horizon ")

# will be replaced by {4: -1, 8: -1, 12: -1, 24: -1, 36: -1, 48: 30, 60: -1, 72: 20, 168: 40}
tp_lst = {4: 0.005, 8: 0.01, 12: 0.015, 24: 0.02, 36: 0.025, 48: 0.03, 60: 0.035, 72: 0.04, 168: 0.05}
# was {4: 0.005,24: 0.01, 48: 0.015, 72: 0.02, 168: 0.03}

selection_nm = 'selected'
week_nm = "week_index"
horizon_nm = "horizon"
first_sel_nm = "modified ratio"
avg_gain_nm = "avg_gain_2w"
total_gain_nm = "gain_2w"
good_alerts_nm = "good_alerts_2w"
total_alerts_nm = "total_alerts_2w"
good_exp_nm = "good_exp_2w"
gain_imp_ratio_nm = "gain_improve_ratio"
max_min_avg_nm = "mx_mn_avg_2w"
tp_sl_criterion_nm = "file"
base_modified_ratio_nm = "ratio_2w"
precision_nm = "precision_2w"
second_sel_nm = avg_gain_nm

metric_improvement_th = 0.65 # minimal precision of added transactions
gain_imp_ratio_th = 1.10    # minimal total_gain_increase/avg_gain_decrease
modified_ratio_th = 0.55
precision_th = 0.55

delta_gain_nm = "delta_gain"
del_metric_nm = "del_good_alrts_to_del_total"

config_fname = r'dataBuyForSelection.csv' # r'dataBuyForSelection.csv'
updated_conf_fname = r'dataBuyWithSelectionMultHz.csv'


def metric_improvement(old_good_alerts, old_total_alerts, new_good_alerts, new_total_alerts, acceptance_threshold):
    """
    logical improvement rule. The added alerts must satisfy the following rule: precision of added alerts >= acceptance_threshold
    :param old_good_alerts:
    :param old_total_alerts:
    :param new_good_alerts:
    :param new_total_alerts:
    :param acceptance_threshold: precision of added alerts should surpass this thresholds
    :return: boolean - rule's decision True = configuration is admissible by this rule
    """

    del_good = new_good_alerts - old_good_alerts
    del_total = new_total_alerts - old_total_alerts

    old_ratio = (old_good_alerts / old_total_alerts) if old_total_alerts>0 else 0
    new_ratio = (new_good_alerts / new_total_alerts) if new_total_alerts>0 else 0

    if del_total <= 0:
        return new_ratio > old_ratio

    imp_ratio = del_good / del_total
    return imp_ratio >= acceptance_threshold


def gain_improvement_ratio(old_gain, old_avg_gain, new_gain, new_avg_gain, ratio_acceptance_threshold):
    """
    the advantage of the total gain/decrease in avg_gain >= ratio_acceptance_threshold
    :param old_gain:
    :param old_avg_gain:
    :param new_gain:
    :param new_avg_gain:
    :param ratio_acceptance_threshold:
    :return: boolean - rule's decision True = configuration is admissible by this rule
    """
    if new_avg_gain >= old_avg_gain:
        return new_gain > old_gain                     # improvement in (almost) both features

    improvement_ratio = (new_gain - old_gain) / (old_avg_gain - new_avg_gain)
    return improvement_ratio >= ratio_acceptance_threshold


def conditional_filter_out_little_active_conf(conf_df):
    """
    weed out low alert configurations, if we have configurations with precision >= precision_th
    :param conf_df: dataFrame of configurations
    :return: filtered configurations dataFrame
    """
    if conf_df[precision_nm].max() >= precision_th:
        filtered_conf_df = conf_df[conf_df[total_alerts_nm] > 2]

        if filtered_conf_df[total_alerts_nm].count()>0 and filtered_conf_df [precision_nm].max() >= precision_th:
            result = filtered_conf_df
        else:
            result = conf_df
    else:
        result = conf_df

    return result


def run_parameter_selection(given_config_df, max_num_of_tx , max_min_avg):
    """
     the main core of the selection mechanisms infrastructure.
     Iterative application of (currently) 3 rules, till the list of remaining configurations is empty.
    :param given_config_df: a dataFrame of configurations
    :param max_num_of_tx: maximal recommended number or alerts. negative --> unlimited
    :param max_min_avg: float - required minimal max average gain (min for sells)
    :return:  a list of configuration rows to be used as a selections for best parmeters for their respective horizon
              and week
    """

    # filter is based on precision_nm. currently calculated as "modified ratio". this may change
    config_df = conditional_filter_out_little_active_conf(given_config_df)

    #config_df = given_config_df

    config_no_more_than_maxtx_df = config_df[config_df[total_alerts_nm] <= max_num_of_tx]

    if (max_num_of_tx > 0) and (config_no_more_than_maxtx_df[first_sel_nm].count() > 0):
        conf_df = config_no_more_than_maxtx_df
    else:
        conf_df = config_df

    if conf_df[first_sel_nm].count()==0:
        return []

    ind_max = int (conf_df.idxmax()[first_sel_nm])
    current_best_conf = conf_df.loc[ind_max]

    # max min avg is identical to all configs per given week and horizon. we can thus use the selected one
    if current_best_conf[max_min_avg_nm] < max_min_avg:
        return []

    conf_df_bck = conf_df

    continue_cnd = True

    while continue_cnd:

        avg_gain = current_best_conf[avg_gain_nm]
        good_alerts = current_best_conf[good_alerts_nm]
        total_alerts = current_best_conf[total_alerts_nm]
        total_gain = current_best_conf[total_gain_nm]
        reported_precision = current_best_conf[first_sel_nm]

        # greedy alg of selection rules
        # total gain must increase
        conf_df[delta_gain_nm] = conf_df[total_gain_nm] > total_gain

        # more or less the precision of the added alerts should be above threshold
        #""" not currently used
        conf_df[del_metric_nm] = conf_df.apply(lambda x: metric_improvement(old_good_alerts=good_alerts,
                                                                            old_total_alerts=total_alerts,
                                                                            new_good_alerts=x[good_alerts_nm],
                                                                            new_total_alerts=x[total_alerts_nm],
                                                                            acceptance_threshold=metric_improvement_th),
                                               axis=1)
                                               
        #"""
        # rule regarding the increase total gain vx. decrease in avg gain
        conf_df[gain_imp_ratio_nm] = conf_df.apply(lambda x: gain_improvement_ratio(old_gain=total_gain,
                                                                                    old_avg_gain=avg_gain,
                                                                                    new_gain=x[total_gain_nm],
                                                                                    new_avg_gain=x[avg_gain_nm],
                                                                                    ratio_acceptance_threshold=gain_imp_ratio_th),
                                                   axis=1)

        to_filter_df = conf_df
        # taking action on selection criteria.
        # an optimized code would filter out non-selected immediately after each criterion is made.
        to_filter_df = to_filter_df[to_filter_df[delta_gain_nm]]
        #to_filter_df = to_filter_df[to_filter_df[del_metric_nm]]
        to_filter_df = to_filter_df[to_filter_df[gain_imp_ratio_nm]]

        continue_cnd = to_filter_df[first_sel_nm].count()>0

        if continue_cnd:
            temp_data_df = to_filter_df[second_sel_nm]
            ind_max = temp_data_df.idxmax()
            current_best_conf = to_filter_df.loc[ind_max]
            conf_df = to_filter_df

    # select all identical configurations
    equivalent_conf_df = conf_df_bck[conf_df_bck[first_sel_nm]==reported_precision]
    equivalent_conf_df = equivalent_conf_df[equivalent_conf_df[total_gain_nm] == total_gain]
    equivalent_conf_df = equivalent_conf_df[equivalent_conf_df[avg_gain_nm] == avg_gain]
    best_conf_lst = equivalent_conf_df.index
    return best_conf_lst


def select_params_for_hz(conf_df, hz, conf_id_lst) -> list:
    """
    filter out records of other horizons and hz and run selection process
    :param conf_df: DataFrame of configurations
    :param hz: int - current horizons
    :param conf_id_lst: a list of selected configurations
    :return:  a list of configuration rows to be used as a selections for best parmeters for their respective horizon
              and week
    """

    configuration_df = conf_df[conf_df[horizon_nm] == hz]

    max_tx = max_allowed_tx[hz]
    required_max_min = tp_lst[hz]
    selected_conf_lst = run_parameter_selection(configuration_df, max_tx, required_max_min)

    conf_id_lst.extend(selected_conf_lst)
    return conf_id_lst


def select_params(conf_df, week, hz_lst, selected_conf_lst: list) -> list:
    """
    given a week and horizon list, and an existing list, select configurations and append their numbers to the give
    list

    :rtype: list
    :param conf_df: dataFrame of configurations. each row is a different configuration
    :param week: a specific week index
    :param hz_lst: list of horizons
    :param selected_conf_lst: list of selected configurations, to append results to.
    :return: the appended list of configurations
    """
    configurations_df = conf_df[conf_df[week_nm] == week]
    conf_source_df = conf_df.copy()

    for hz in hz_lst:
        conf_source_df = configurations_df.copy()
        selected_conf_lst = select_params_for_hz(conf_source_df, hz, selected_conf_lst)

    return selected_conf_lst


def read_configurations(conf_filename: str) -> pd.DataFrame:
    """
    read csv of configurations to process
    :param conf_filename:
    :return: dataFrame of all configurations
    """
    conf_df = pd.read_csv(conf_filename) #,  index_col=0)
    # add a selection column
    conf_df[ selection_nm ] = 0
    return conf_df


def perform_preliminary_filtering(data_df):
    """
    remove configs (days) whose gain_2w is negative
    remove config lines whose week_index ==0
    :param data_df: dataFrame all read configurations
    :return: filtered configuration
    """
    data2_df = data_df[data_df[week_nm] != 0]
    data2_df = data2_df[data2_df[total_gain_nm] >= 0.0]
    #data2_df = data2_df.reindex(copy=False)
    return data2_df

    """
        def modified_ratio(x_df):
        
        #calculate modified ratio - the first attribute to take the maximum of
        #:param x_df:
        #:return:
        
        # currently using a simple rule (x_df[good_alerts_nm] + x_df[good_exp_nm]   )/x_df[total_alerts_nm]
        
        result = x_df[base_modified_ratio_nm] if x_df[tp_sl_criterion_nm] == 110.0 else ((8.0 * x_df[base_modified_ratio_nm] + 3.0) / 11.0)
        return result
    """


def process_file(input_file=config_fname, output_file=updated_conf_fname, weeks_rn=-1, horizons=horizon_lst):
    """
    main function: process a file and add a 'selected' column if its week and horizons are treated and the configuration
    is found to be optimal
    :param input_file:
    :param output_file:
    :param weeks_rn: range of weeks. default is all weeks that can be found in input file
    :param horizons: list of horizons
    :return: None
    """
    data_df = read_configurations(config_fname)
    data_df = perform_preliminary_filtering(data_df)

    print("before apply")
    data_df[first_sel_nm] = data_df[base_modified_ratio_nm]
    data_df[precision_nm]= (data_df[good_alerts_nm] + data_df[good_exp_nm]) / data_df[total_alerts_nm]
        #data_df.apply(modified_ratio, axis=1)

    print("current records = ", data_df[first_sel_nm].count())
    data_df = data_df[data_df[first_sel_nm] >= modified_ratio_th ]
    print("after removing bad modified ratio, records = ", data_df[first_sel_nm].count())

    print("after apply")

    if weeks_rn == -1:
        first_week = data_df[week_nm].min()
        last_week = data_df[week_nm].max()
        weeks_range = range(first_week, last_week + 1)
        print("weeks ",first_week," till ", last_week, " are to be processed")
    else:
        weeks_range = weeks_rn

    selected_config_lst = []
    for current_week in weeks_range:
        print ("processing week ",current_week)
        selected_config_lst = select_params(data_df, current_week, horizons, selected_config_lst)

    # set selected values to one
    for conf_ind in selected_config_lst:
        data_df.at[conf_ind, selection_nm] = 1

    output_df = data_df[data_df[selection_nm] == 1]
    print("Parameters selction is now saved to file:", updated_conf_fname)
    output_df.to_csv(updated_conf_fname)
    # was data_df.to_csv(updated_conf_fname)
    return

#######################################################################################################################
#    main

#process_file(weeks_rn = [44], horizons= [8])
""",horizons = [168]"""

process_file()
print('first processing is over')

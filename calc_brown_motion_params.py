import scipy
import numpy as np
import pandas as pd
import datetime
from stoppingtime import *
from numpy import sqrt
import matplotlib

# from scipy import interpolate
# from scipy.interpolate import CubicSpline

# import operator

MAX_VOL_WINDOW = 336 * 60
MIN_VOL_WINDOW = 12 * 60

start_date = '2017-07-01 00:00:00'
end_date = '2017-12-15 00:00:00'

data = pd.read_parquet("W:\\Dim sum\\vol_data.parquet", engine='pyarrow')
data['ln_price'] = np.log(data['price'])
data['ln_price_shift'] = data['ln_price'].shift(+1)
data['ln_diff_sq'] = (data['ln_price'] - data['ln_price_shift']) ** 2


# zz= data.truncate(before = start_date, after = end_date)
# zz = data[ (data['info_datetime'] >=  start_date   ) & (data['info_datetime'] <  end_date)   ]
# print(data)

def vol_win_len(horizon_minutes) -> int:
    """
    return the appropriate window from which to estimate volatility and drift
    :param horizon_minutes: required horizon for Expected value calculation
    :return: volatility window length in minute
    """
    return max(min(3 * horizon_minutes, MAX_VOL_WINDOW), MIN_VOL_WINDOW)


def get_gmb_params(price_df, calculation_date, hzn_in_minutes) -> (float, float, float, float):
    """
    estimate the geometric brownian movement parameters calculated for a date with the appropriate volatility window
    :param price_df: price dataFrame
    :param calculation_date: date for which the vol \sigma and drift \mu\sigma^2 are required
    :return: subFrame with usefull data for window
    """
    vol_win = vol_win_len(hzn_in_minutes)
    # print("win minutes= ", vol_win, " = ", vol_win/60, " hours")
    end_indx_of_win = price_df.index[price_df['info_datetime'] == calculation_date].tolist()[-1]
    start_indx_of_win = end_indx_of_win - vol_win

    sub_df = price_df.iloc[start_indx_of_win:(end_indx_of_win + 1)]
    lnvt0 = sub_df.loc[start_indx_of_win].ln_price
    lnvtn = sub_df.loc[end_indx_of_win].ln_price
    nusigmaSquared_estimate = (lnvtn - lnvt0) / vol_win
    ex_sq_ln = sub_df.loc[(start_indx_of_win + 1):(end_indx_of_win + 1)]['ln_diff_sq'].mean()
    sigma_squared = ex_sq_ln - nusigmaSquared_estimate * nusigmaSquared_estimate
    assert sigma_squared > 0, "volatility should be non-negative"
    volatility = np.sqrt(sigma_squared)
    nu = nusigmaSquared_estimate / sigma_squared
    mu_drift = nusigmaSquared_estimate + 0.5 * sigma_squared
    return volatility, nusigmaSquared_estimate, nu, mu_drift


def test_scale(horizon, iters=1000, digits=100 ):
    scales = [4.0, 0.01, 0.1, 0.5, 0.8, 0.9, 1.0, 2.0, 3.0, 4.0, 8.0, 12.0, 16.0]
    mp.dps = digits

    normalized_drift = -43.10760369550707  # 34 # 4
    vol = 0.09 #0.001012048268027361  # 0.01/2

    for scale in scales:
        minutes_in_hour = mpf(60.0)
        hzn_in_minutes = minutes_in_hour * mpf(horizon)

        sigma_in_h_scale = mpf(scale)
        derived_time_scale = mpf(1.0) / sigma_in_h_scale ** 2
        scaled_horizon = mpf(hzn_in_minutes) * derived_time_scale

        scaled_sigma = mpf(vol) * sigma_in_h_scale

        in_h_func = StoppingTime(TP=tp_level, SL=sl_level, horizon=scaled_horizon, nu=normalized_drift,
                                 sigma=scaled_sigma, iterations=iters)

        #   StoppingTime(TP = TP, SL=SL, horizon=horizon, nu=nu, sigma=sigma, iterations=iterations)
        res_till_h_normalized = in_h_func.in_h_EX() / (minutes_in_hour * derived_time_scale)  # part of an hour

        prob_exp, res_from_h = in_h_func.from_h_prob()
        res_from_h_normalized = res_from_h / (minutes_in_hour * derived_time_scale)
        total = res_till_h_normalized + res_from_h_normalized

        print("h = ", horizon, " scaling=", scale, "EX in h = ", float(res_till_h_normalized ), "EX from h = ", float(res_from_h_normalized), " Prob EXP = ", float(prob_exp),
              "  total = ", float(total) )

    return

#comment added
test_vec = [10, 30, 60, 240, 480, 720, 1440, 2160, 2880, 3600, 4320, 10080]
test_vec_unrefined = [0.1 * 60, 0.5 * 60, 4 * 60, 24 * 60, 48 * 60, 72 * 60, 168 * 60]
minutes_in_hours = 60
noIter = 5000
scaling = 1000000.0
seconds_per_day = 24 * 60
tp_level = 1.005
sl_level = 1/tp_level # 0.998

#mp.dps = 20
#test_scale(4, digits= 50, iters=noIter)
#assert 1 == 3, "end"
#regular code
if False:
    print(" date of ", end_date)
    for win in test_vec_unrefined:
        hzn_in_hours = win / 60
        vol, numsigSq, normalized_drift, mu = get_gmb_params(data, end_date, win)  # Ilan! should change to    "win"
        # print ("Window = ", win/60, " hours")
        print()
        print("Horizon = ", hzn_in_hours, " vol = ", vol, " nu = ", normalized_drift, "  Drift per [minute, hour, day] mu  = ",
              mu, ", ", mu * 60, " , ", mu * seconds_per_day)
        lin_drift = normalized_drift * vol ** 2
        print("nuSig^2 = ", lin_drift, " sigma = ", vol, " lin drift per [minute, hour, day] = [", lin_drift, ", ",
              lin_drift * minutes_in_hours, " ,", lin_drift * minutes_in_hours * 24, "]")

        total_expected_time, res_in_h, res_from_h, prob_of_expire, exp_future_val = calc_in_and_out_h_EX(TP=tp_level, SL=sl_level,
                                                                                    horizon=hzn_in_hours,
                                                                                    normalized_drift=normalized_drift,
                                                                                    sigma=vol,
                                                                                    basic_no_of_iters=noIter)
        print("Hzn = ", hzn_in_hours, " in [0,h] = ", res_in_h, "\nprobExp = ", prob_of_expire, "\nin [h,inf) EX = ", res_from_h,
              "\nExpectedT = ", total_expected_time, "\nExpected fut val = ", exp_future_val)




#    vol, numsigSq, nu, mu =  get_gmb_params(data, end_date, 241)

#    print( "vol = ", vol)
#    print( "Drift per minute mu  = ", mu)
#    print( " drift per day =", mu*24*60)

# 0 = diffusion
# 1 = drift
diffusion_or_drift = 2
# drift dominated scenario
if diffusion_or_drift==1:
    print("Approximating drift")
    nu = +499.5
    vol = 0.001
    gain = 0.02
    hzn_in_hours = 100
    mu = (nu + 0.5) * vol ** 2
    noIter = 1000
    # desired_t = gain/abs(mu)
elif diffusion_or_drift==0:# volatility dominated scenario
    print("Approximating diffusion only")
    vol = 0.01
    nu = 0.001 # -20
    mu = (nu + 0.5)*vol**2
    gain = 0.1 # 201
    hzn_in_hours = 1000
    noIter = 5000
elif diffusion_or_drift==2:
    print("Approximating moderate drift")
    nu = +40.5
    vol = 0.004
    gain = 0.02
    hzn_in_hours = 0.3
    mu = (nu + 0.5) * vol ** 2
    noIter = 1000





desired_drift_t = ln(1+gain)/ln(1+mu)    # for drift controled process
desired_diffus_t = (gain ** 2) / (2 * vol ** 2 * (0.5 + nu))

tp_level = 1+gain
sl_level = 1/tp_level

normalized_drift = nu


for hzn_in_hours in [10, 100,1000,10000]:
    print("\n")
    print("Bonus:Horizon = ", hzn_in_hours, " vol = ", vol, " nu = ", normalized_drift, "  Drift per [minute, hour, day] mu  = ",
          mu, ", ", mu * 60, " , ", mu * seconds_per_day)

    lin_drift = (normalized_drift +.5)* vol ** 2
    print("nuSig^2 = ", lin_drift, " sigma = ", vol, " lin drift per [minute, hour, day] = [", lin_drift, ", ",
          lin_drift * minutes_in_hours, " ,", lin_drift * minutes_in_hours * 24, "]")
    print("Desired diffusion only time = ", desired_diffus_t)
    print("Desired drift only time = ", desired_drift_t)

    total_expected_time, res_in_h, res_from_h, prob_of_expire, exp_future_val = calc_in_and_out_h_EX(TP=tp_level, SL=sl_level,
                                                                                     horizon=hzn_in_hours,
                                                                                     normalized_drift=normalized_drift,
                                                                                     sigma=vol,
                                                                                     basic_no_of_iters=noIter, no_of_decimals=200)

    print("Bonus: Hzn = ", hzn_in_hours, " in [0,h] = ", res_in_h*60.0, 'min ',"\nprobExp = ", prob_of_expire, "\nin [h,inf) EX = ",
          res_from_h*60.0, ' min ',
          "\nExpectedT = ", total_expected_time*60.0, " min\n","Expected future val =", exp_future_val)


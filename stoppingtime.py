__author__ = "Ilan Zohar"
__credits__ = [ "Ilan Zohar"]
__version__ = "1.0.0"
__status__ = "Prototype"
"""Expected time calculation of a Geometric Brownian Motion ending in a stopping time of first hit of TP or SL or Exp

:keywords Geometric, Brownian, Motion
Sparks AB 05/06/2019
"""
from mpmath import *

def clip(val, lower_bound, upper_bound):
    """
    clip value by bounds from above and below
    :param val: to be clipped
    :param lower_bound:
    :param upper_bound:
    :return: clipped value
    """
    clipped_val = max(val, lower_bound)
    clipped_val = min(clipped_val, upper_bound)
    return clipped_val

class StoppingTime:
    def __init__(self, TP, SL, horizon, nu, sigma, iterations=10000):
        """

        :param TP: take profit ratio. 1.05 = 5 percents profit
        :param SL: stop loss ratio. 0.95 = 5 percent loss
        :param hzn: horizon [minutes]
        :param nu: normalized drift of the geometric brownian motion (GBM). [delta(gain)/min])
        :param sigma: volatility of the GMB process. [del(gain)^2/min^2]
        :param iterations: half the number of iterations around the base (for the series evaluation)
        """
        assert (TP>1)
        assert (SL<1)
        assert horizon>0
        assert iterations > 0
        self.TP = mpf(TP)
        self.SL = mpf(SL)
        self.h  = mpf(horizon)
        self.nu = mpf(nu)
        self.sigma = mpf(sigma)
        self.iterations = iterations

        self.sqrt_pi = sqrt(pi)
        sqrt_two_pi = (self.sqrt_pi*mpf(sqrt(2.0)) )

        # parameters used in the calculation of the formulae
        self.AC = ((self.SL ** self.nu)/sqrt_two_pi, (self.TP ** self.nu )/sqrt_two_pi)
        #to_be_squared = self.nu*self.sigma
        self.B = fabs(self.nu)*self.sigma #    to_be_squared*to_be_squared
        self.D_free = (log(mpf(1.0) / self.SL) / self.sigma, log(self.TP) / self.sigma)
        self.D_to_be_multiplied_by_2k = log(self.TP / self.SL) / self.sigma
        self.sqrt_h = sqrt(self.h)

    def calc_stopping_time(self):
        """
            Let T(w) be the first moment such that the GMB process hits TP or WL or expires.
            This carry out the actual calculation of the expected value of the stopping time T(w).
            Use the probability density of a geometric brownian motion starting at 1.0.
            calcls symultanously the integral of t*density(t) in [0,h] and the probability
            i.e. the integral of density in [h,+ing).
            Refer to a written report for details.
        :return: total_expected_time, exp_time_of_bounds_hitting_brownian_motion, exp_time_of_from_h, prob_of_expire
        """
        exp_time_of_bounds_hitting_brownian_motion = mpf(0.0)  # expected Time or the GBM on the event that the process hits either TP or SL (before the horizon)
        prob_of_expire = mpf(0.0)  # probability that the process never hits neither TP nor SL

        for i in range(0, 2):  # 0 stands for SL (StopLoss), 1 for TP (Take Profit)
            for k in range(-self.iterations, self.iterations + 1):
                d_k_i = self.D_free[i] + mpf(2 * k) * self.D_to_be_multiplied_by_2k
                i_k_i_coeff = self.sqrt_pi * sqrt(mpf(0.5)) * self.AC[i] * (d_k_i / self.B)
                # correct sign of d_k_i must be used in i_k_i unlike below where the abs thereof is to be used
                pos_exp = exp(self.B * fabs(d_k_i))
                base = self.B * self.sqrt_h / sqrt(mpf(2.0))
                offset = fabs(d_k_i) / (self.sqrt_h * sqrt(mpf(2.0)))

                first_naked_erfc = erfc(base - offset)
                # calc for in [SL,TP} ex time
                first_erfc = (mpf(2) - first_naked_erfc)
                second_erfc = erfc(base + offset)
                first_addend = first_erfc / pos_exp
                second_addend = -second_erfc * pos_exp

                # sum for [0,h] expected time portion
                i_k_i = i_k_i_coeff * (first_addend + second_addend)
                exp_time_of_bounds_hitting_brownian_motion += (0 if isnan(i_k_i) else i_k_i)

                # probability calculation for Expired process event
                # sign is required since it is the division outcome of  d_k_i/abs(d_k_i)
                j_k_i_coeff = self.sqrt_pi * sqrt(mpf(0.5)) * self.AC[i] * sign(d_k_i)
                first_addend_exp = first_naked_erfc / pos_exp
                second_addend_exp = second_addend  # the same as above
                j_k_i = j_k_i_coeff * (first_addend_exp + second_addend_exp)
                prob_of_expire += j_k_i

        #make errors more bearable :) prob should be between 0 and 1. (it is however only estimated here)
        prob_of_expire = clip( prob_of_expire, upper_bound=mpf(1), lower_bound=mpf(0))
        # time of bounds hitting process should be no less than zero, no more then the horizon
        exp_time_of_bounds_hitting_brownian_motion = clip(exp_time_of_bounds_hitting_brownian_motion, upper_bound=self.h, lower_bound=0)

        exp_time_of_from_h = prob_of_expire*self.h
        total_expected_time = exp_time_of_bounds_hitting_brownian_motion + exp_time_of_from_h
        return total_expected_time, exp_time_of_bounds_hitting_brownian_motion, exp_time_of_from_h, prob_of_expire


def calc_in_and_out_h_EX(TP: float, SL: float, horizon: float, normalized_drift: float, sigma: float, basic_no_of_iters: int = 1000, no_of_decimals: int = 50) -> object:
    """
        calc expected stopping time of a geometric brownian motion with stops in TP or SL or expiration=horizon.
        calc also probability of expiration
        This function configures the calculation object and subsequently invokes the calculation method.

    :param TP: take profit. a number 1.05 means 5% profit
    :param SL: stop loss. a number - 0.95 means 5% loss
    :param horizon: HOURS till expiration
    :param normalized_drift. describes the local price trend. data is based on by minute based calculation.
    :param sigma: volatility. given by minute based units.
    :param basic_no_of_iters: number of calc iterations. suitable for normal nu
    :param no_of_decimals: precision of used floats
    :return:  total_expected_time (hours),
              expected stoping_time[hours] in [0,hzn],
              expected stopping_time[hours] in [hzn,+inf),
              expiration probability,
              expected_future_val (number) - expected value of the GBM at time of stop.
    """
    mp.dps = no_of_decimals

    # the model cannot entertain zero normalized drift. a very small nu is used instead
    if normalized_drift==0:
        nu = 0.00001
    else:
        nu = normalized_drift

    # higher precision is required in smaller nu
    if abs(nu)<1:
        noOfIters = 5*basic_no_of_iters
    else:
        noOfIters = basic_no_of_iters

    # the curve model's parameters are given by per minute units. (the horizon in given in hours)
    minutes_in_hour = mpf(60.0)
    hzn_in_minutes = minutes_in_hour*mpf(horizon)

    # scaling is supported but not used (as it is not necessary when using mpf)
    sigma_in_h_scale = mpf(4.0)
    derived_time_scale = mpf(1.0)/sigma_in_h_scale**2
    scaled_horizon = hzn_in_minutes*derived_time_scale
    scaled_sigma = mpf(sigma)*sigma_in_h_scale

    #there is no actual good reason for splitting the configuration of the object and its execution
    stop_time_obj = StoppingTime(TP, SL, horizon=scaled_horizon, nu=nu, sigma= scaled_sigma, iterations= noOfIters)
    total_exp_time, exp_time_before_hzn, exp_time_of_expiration, prob_of_expiration = stop_time_obj.calc_stopping_time()
    #rescaling to hours

    minutely_drift = (normalized_drift +mpf(0.5))*sigma**2
    expected_future_val = exp(minutely_drift*total_exp_time)

    return total_exp_time/minutes_in_hour, exp_time_before_hzn/minutes_in_hour, exp_time_of_expiration/minutes_in_hour, prob_of_expiration, expected_future_val


######################################################################################################################
#    test code
if __name__ == "__main__":
    sigma_scale=1
    mp.dps = 50
    #getcontext().prec = 200
    #print(Decimal('3.14')/Decimal(3) )
    tp_level = 1.005 #1.01
    sl_level = 1 / tp_level

    # per minute vol and normalized drift
    volatility = 0.001012048268027361  # 0.001
    normalized_drift =  0.001 # -43.10760369550707#  50
    for hzn_in_hours in [168, 0.1, 0.5, 1, 4, 24, 48, 72, 168]:

        noIter = 5000

        # hours scaling
        #total_expected_time, res_in_h, res_from_h, prob_of_expire = in_and_out_h_EX(TP=tp_level, SL=sl_level, horizon=hzn_in_hours, nu=normalized_drift, sigma=volatility*sqrt(60) , iterations=noIter)
        #print("\nHzn = ", hzn_in_hours, " in [0,h] = ", res_in_h, "\nprobExp = ", prob_of_expire, "\nin [h,inf) EX = ", res_from_h, "\nExpectedT = ", total_expected_time )

        #minute scaling
        #time_scale = 1/(sigma_scale**2.0)
        #hzn_minutes = hzn_in_hours*60
        #total_expected_time, res_in_h, res_from_h, prob_of_expire = in_and_out_h_EX(TP=tp_level, SL=sl_level, horizon=hzn_minutes*time_scale, nu=normalized_drift, sigma=volatility*sigma_scale ,
        #                                                                            iterations=noIter)

        #print("\nHzn = ", hzn_in_hours, " in [0,h] = ", (res_in_h/60)/time_scale, "\nprobExp = ", prob_of_expire, "\nin [h,inf) EX = ", (res_from_h/60)/time_scale, "\nExpectedT = ",
        #      (total_expected_time/60)/time_scale )

        total_expected_time, res_in_h, res_from_h, prob_of_expire, future_gain = calc_in_and_out_h_EX(TP=tp_level, SL=sl_level,
                                                                                    horizon=hzn_in_hours,
                                                                                    normalized_drift=normalized_drift,
                                                                                    sigma=volatility,
                                                                                    basic_no_of_iters=10000)

        print("\nopt:Hzn = ", hzn_in_hours, " in [0,h] = ", res_in_h, "\nprobExp = ", prob_of_expire, "\nin [h,inf) EX = ", res_from_h, "\nExpectedT = ", total_expected_time )
